FROM alpine:3.10

RUN apk add ca-certificates gettext

RUN wget -O /tmp/terraform.zip https://releases.hashicorp.com/terraform/0.12.4/terraform_0.12.4_linux_amd64.zip
RUN unzip /tmp/terraform.zip -d /usr/bin/
